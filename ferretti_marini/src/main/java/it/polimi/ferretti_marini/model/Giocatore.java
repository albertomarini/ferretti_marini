package it.polimi.ferretti_marini.model;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;

public class Giocatore {
	
	private EnumColore colore;
	private int posizione [] = new int[2];
	private String nome;
	private int soldi;
	private List<TesseraTerreno> listaTessereTerreno = new ArrayList<TesseraTerreno>();
	
	public Giocatore(String nome, EnumColore colore, int posizione1, int posizione2, int soldi){
		this.nome = nome;
		this.colore = colore;
		this.soldi = soldi;
		posizione[0] = posizione1;
		posizione[1] = posizione2;
	}
	
	public void setPosizione(int i, int arg) {
		posizione[i] = arg;
	}
	
	public int getPosizione(int i) {
		return posizione[i];
	}
	
	public EnumColore getColore() {
		return colore;
	}

	public void setColore(EnumColore colore) {
		this.colore = colore;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public int getSoldi() {
		return soldi;
	}
	
	public void setSoldi(int soldi) {
		this.soldi = soldi;
	}
	
	public void addTesseraTerreno(TesseraTerreno tesseraTerreno){
		listaTessereTerreno.add(tesseraTerreno);
	}
	
	public int numeroDeserti(){
		int i = 0;
		Iterator<TesseraTerreno> iterator = listaTessereTerreno.iterator();
		while(iterator.hasNext()){
			if((iterator.next()).getTipologia() == EnumRegione.DESERTO) {
				i++;
			}
		}
	return i;
	}
	
	public int numeroPianure(){
		int i = 0;
		Iterator<TesseraTerreno> iterator = listaTessereTerreno.iterator();
		while(iterator.hasNext()){
			if((iterator.next()).getTipologia() == EnumRegione.PIANURA){ 
				i++;
			}
		}
	return i;
	}
	
	public int numeroLaghi(){
		int i = 0;
		Iterator<TesseraTerreno> iterator = listaTessereTerreno.iterator();
		while(iterator.hasNext()){
			if((iterator.next()).getTipologia() == EnumRegione.LAGO){
				i++;
			}
		}
	return i;
	}
	
	public int numeroForeste(){
		int i = 0;
		Iterator<TesseraTerreno> iterator = listaTessereTerreno.iterator();
		while(iterator.hasNext()){
			if((iterator.next()).getTipologia() == EnumRegione.FORESTA){
				i++;
			}
		}
	return i;
	}
	
	public int numeroColline(){
		int i = 0;
		Iterator<TesseraTerreno> iterator = listaTessereTerreno.iterator();
		while(iterator.hasNext()){
			if((iterator.next()).getTipologia() == EnumRegione.COLLINA){
				i++;
			}
		}
	return i;
	}
	
	public int numeroMontagne(){
		int i = 0;
		Iterator<TesseraTerreno> iterator = listaTessereTerreno.iterator();
		while(iterator.hasNext()){
			if((iterator.next()).getTipologia() == EnumRegione.MONTAGNA){
				i++;
			}
		}
	return i;
	}
	
}