package it.polimi.ferretti_marini.model;

import java.util.Iterator;
import java.util.List;
import java.util.Scanner;
import java.util.ArrayList;
import java.io.File;
import java.io.IOException;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.jgrapht.graph.SimpleGraph;
import org.jgrapht.UndirectedGraph;

/**
 * Classe il cui obbiettivo è racchiudere come suoi campi tutte le strutture dati che contengono gli oggetti da
 * creare, interrogare e aggiornare nel corso della partia. Contine:
 * 		numeroGiocatori: parametro che indica il numero di giocatori che stanno giocando
 * 		giocatori: arrayList di tipo Giocatore contenente gli oggetti associati ad ogni giocatore
 * 		regioni: arrayList di tipo Regione contenente gli oggetti associati ad ogni regione
 * 		strade: arrayList di tipo Strada contenente gli oggetti associati ad ogni strada
 * 		tessereTerreno: vettore di tipo int di 6 elementi ognugno dei quali rappresenta il costo della
 * 			prossima tessera terreno.
 * 		disponibilitaRecinto: rappresenta il numero di recinti disponibili rimasti
 * 		disponibilitaRecintoFinale: rappresenta il numero di recinti finali disponibili rimasti
 *		g: grafo rappresentante la mappa i cui nodi sno oggetti di tipo regione e i cui archi sono oggetti di tipo
 *			strada
 *		disponibilitaMosse: array di tipo int necessario immagazzinare l'informazione relativa a quali mosse sono gia
 *			state effettuate durante il turno di un determinato giocatore. 
 *
 * @author Valerio Ferretti e Alberto Marini
 *
 */
public class StrutturaDati{
	
	private int numeroGiocatori;
	public List<Giocatore> giocatori = new ArrayList<Giocatore>();
	public List<Regione> regioni = new ArrayList<Regione>();
	public List<Strada> strade = new ArrayList<Strada>();
	private int[] tessereTerreno = {0,0,0,0,0,0};
	private int disponibilitaRecinto = 20;
	private int disponibilitaRecintoFinale = 12;
	public UndirectedGraph<Regione,Strada> g = new SimpleGraph<>(Strada.class);
	public int[] disponibiltaMosse = new int[3];
			
	public int getCostoTesseraTerreno(EnumRegione tipologia) {
		if(tipologia == EnumRegione.DESERTO) {
			return tessereTerreno[0];
		}
		if(tipologia == EnumRegione.PIANURA) {
			return tessereTerreno[1];
		}
		if(tipologia == EnumRegione.LAGO) {
			return tessereTerreno[2];
		}
		if(tipologia == EnumRegione.FORESTA) {
			return tessereTerreno[3];
		}
		if(tipologia == EnumRegione.COLLINA) {
			return tessereTerreno[4];
		} else {
			return tessereTerreno[5];
		}
	}
		
	public void incrementaCostoTessereTerreno(EnumRegione tipologia) {
		if(tipologia == EnumRegione.DESERTO) {
			tessereTerreno[0] =+ 1;
		}
		if(tipologia == EnumRegione.PIANURA) {
			tessereTerreno[1] =+ 1;
		}
		if(tipologia == EnumRegione.LAGO) {
			tessereTerreno[2] =+ 1;
		}
		if(tipologia == EnumRegione.FORESTA) {
			tessereTerreno[3] =+ 1;
		}
		if(tipologia == EnumRegione.COLLINA) {
			tessereTerreno[4] =+ 1;
		}
		if(tipologia == EnumRegione.MONTAGNA) {
			tessereTerreno[5] =+ 1;
		}
	}
	
	public int getDisponibilitàRecinto() {
		return disponibilitaRecinto;
	}
	
	public void decrementaDisponibilitàRecinto() {
		disponibilitaRecinto = disponibilitaRecinto-1;
	}
	
	public int getDisponibilitaRecintoFinale() {
		return disponibilitaRecintoFinale;
	}
	
	public void decrementaDisponibilitaRecintoFinale() {
		disponibilitaRecintoFinale = disponibilitaRecintoFinale-1;
	}
			
	public int getNumeroGiocatori() {
		return numeroGiocatori;
	}

	public void incrementaNumeroGiocatori() {
		numeroGiocatori = numeroGiocatori + 1;
	}

	/*Soldi è stato aggiunto come parametro del costrutture per comprendere il caso in cui giocano due giocatori
	  e i soldi non devo essere 20 ma 30*/
	public void addGiocatore(String nome, EnumColore colore, int posizione1, int posizione2, int soldi){
		Giocatore giocatore = new Giocatore(nome, colore, posizione1, posizione2, soldi);
		giocatori.add(giocatore);
	}
	
	/**
	 * Metodo che effettua il parsing di un file xml contenete le informazioni necessarie per inizializzare 
	 * gli oggetti regione che comporrando la mappa. Aggiunge tali oggetti nell'arrayList regioni.
	 * 
	 * @author Valerio Ferretti e Alberto Marini
	 */
	public void xmlToRegioni() throws JDOMException, IOException{
		
		try{
		
			SAXBuilder builder = new SAXBuilder();
			Document document = builder.build(ClassLoader.getSystemResourceAsStream("mappa_regioni.xml"));
			Element rootElement = document.getRootElement();
			//Lista elementi regione
			List<Element> children = rootElement.getChildren(); 
			Iterator<Element> iterator = children.iterator(); 
			while (iterator.hasNext()){ 
				Element element = (Element)iterator.next();
			
				Regione r = new Regione();
			
				r.setId(Integer.parseInt(element.getChildText("id")));
				r.setNumeroPecore(Integer.parseInt(element.getChildText("numeropecore")));
				r.setPresenzaPecoraNera(Boolean.valueOf(element.getChildText("presenzapecoranera")));
				r.setTipologia(EnumRegione.valueOf(element.getChildText("tipologia")));
			
				regioni.add(r);
			}
	
		} catch (JDOMException e){
			System.out.println(e.getMessage());
		} catch (IOException e){
			System.out.println(e.getMessage());
		}
	}

	/**
	 * Metodo che effettua il parsing di un file xml contenete le informazioni necessarie per inizializzare 
	 * gli oggetti strada che comporrando la mappa. Aggiunge tali oggetti nell'arrayList strade.
	 * 
	 * @author Valerio Ferretti e Alberto Marini
	 */
	public void xmlToStrade() throws JDOMException, IOException{
		
		try{
		
			SAXBuilder builder = new SAXBuilder();
			Document document = builder.build(ClassLoader.getSystemResourceAsStream("mappa_strade.xml"));
			Element rootElement = document.getRootElement();
			//Lista elementi regione
			List<Element> children = rootElement.getChildren(); 
			Iterator<Element> iterator = children.iterator(); 
			while (iterator.hasNext()){ 
				Element element = (Element)iterator.next();
			
				Strada s = new Strada();
			
				s.setId(Integer.parseInt(element.getChildText("id")));
				s.setNumero(Integer.parseInt(element.getChildText("numero")));
				s.setStato(EnumStrada.valueOf(element.getChildText("stato")));
			
				strade.add(s);
			}
	
		} catch (JDOMException e){
			System.out.println("Throws JDOMException");
		} catch (IOException e){
			System.out.println("Throws IOException");
		}
	}

	/**
	 * Metodo che effettua il parsing di un file xml contenete le informazioni che traducono la proprieta di
	 * due regioni di essere confinanti e la proprieta di una strada di separare due regioni. Il metodo istanzia
	 * il grafo utilizzando come nodi gli oggetti Regione e come archi gli oggetti Strada prendendo tali oggetti
	 * rispettivamente dall'arrayList regioni e dall'arrayList strade.
	 * 
	 * @author Valerio Ferretti e Alberto Marini
	 */
	public void xmlToGrafo() throws JDOMException, IOException{
		try{
		
			Iterator<Regione> iteratorRegioni = regioni.iterator();
		
			while(iteratorRegioni.hasNext()){
				g.addVertex(iteratorRegioni.next());
			}
		
			SAXBuilder builder = new SAXBuilder();
			Document document = builder.build(ClassLoader.getSystemResourceAsStream("confini.xml"));
			Element rootElement = document.getRootElement();
			List<Element> children = rootElement.getChildren(); 
			Iterator<Element> iterator = children.iterator(); 
		
			while (iterator.hasNext()){ 
			
				Element element = (Element)iterator.next();
			
				int idRegione1 = Integer.parseInt(element.getChildText("regione1"));
				int idRegione2 = Integer.parseInt(element.getChildText("regione2"));
				int idStrada = Integer.parseInt(element.getChildText("strada"));
									
				Iterator<Strada> iteratorStradeXml = strade.iterator();
				Iterator<Regione> iteratorRegioniXml1 = regioni.iterator();
				Iterator<Regione> iteratorRegioniXml2 = regioni.iterator();
				while(iteratorRegioniXml1.hasNext()){
					Regione regione1 = iteratorRegioniXml1.next();
					while(iteratorRegioniXml2.hasNext()){
						Regione regione2 = iteratorRegioniXml2.next();
						while(iteratorStradeXml.hasNext()){
							Strada strada = iteratorStradeXml.next();
							if(regione1.equals(idRegione1) && regione2.equals(idRegione2)&&strada.equals(idStrada)){
								g.addEdge(regione1, regione2, strada);
							}	
						}
					}
				}
			}
		} catch (JDOMException e){
			System.out.println("Throws JDOMException");
		} catch (IOException e){
			System.out.println("Throws IOException");
		}
	}		
	
}