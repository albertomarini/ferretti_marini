package it.polimi.ferretti_marini.model;

public enum EnumRegione {
	
	DESERTO, PIANURA, LAGO, FORESTA, COLLINA, MONTAGNA, SHEEPSBURG;

}