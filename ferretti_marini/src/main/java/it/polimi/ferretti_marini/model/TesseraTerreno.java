package it.polimi.ferretti_marini.model;

public class TesseraTerreno {
	
	public TesseraTerreno(EnumRegione tipologia, int costo){
		this.tipologia = tipologia;
		this.costo = costo;
	}

	private EnumRegione tipologia;
	private int costo;
	
	public EnumRegione getTipologia() {
		return tipologia;
	}
	public void setTipologia(EnumRegione tipologia) {
		this.tipologia = tipologia;
	}
	public int getCosto() {
		return costo;
	}
	public void setCosto(int costo) {
		this.costo = costo;
	}
	
}