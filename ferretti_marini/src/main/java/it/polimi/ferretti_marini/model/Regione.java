package it.polimi.ferretti_marini.model;

public class Regione {
		
	private int id;
	private int numeroPecore;
	private boolean presenzaPecoraNera;
	private EnumRegione tipologia;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getNumeroPecore() {
		return numeroPecore;
	}
	public void setNumeroPecore(int numeroPecore) {
		this.numeroPecore = numeroPecore;
	}
	public boolean isPresenzaPecoraNera() {
		return presenzaPecoraNera;
	}
	public void setPresenzaPecoraNera(boolean presenzaPecoraNera) {
		this.presenzaPecoraNera = presenzaPecoraNera;
	}
	public EnumRegione getTipologia() {
		return tipologia;
	}
	public void setTipologia(EnumRegione tipologia) {
		this.tipologia = tipologia;
	}
	public Boolean equals(Integer id){
		if(this.id == id){ 
			return true;
		} else {
			return false;
		}
	}
	public int hashCode(){
		return super.hashCode();
	}
}