package it.polimi.ferretti_marini.model;

public class Strada{

	private int id;
	private int numero;
	private EnumStrada stato;

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getNumero() {
		return numero;
	}
	public void setNumero(int numero) {
		this.numero = numero;
	}
	public EnumStrada getStato() {
		return stato;
	}
	public void setStato(EnumStrada stato) {
		this.stato = stato;
	}
	
	public Boolean equals(Integer id){
		if(this.id  == id){
			return true;
		} else {
			return false;
		}
	}
	public int hashCode(){
		return super.hashCode();
	}
	
}