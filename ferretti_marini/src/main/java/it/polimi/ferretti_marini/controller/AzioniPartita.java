package it.polimi.ferretti_marini.controller;

import it.polimi.ferretti_marini.model.*;
import java.util.*;

/**
 * La classe AzioniPartita contiene tutti i metodi che implementano le principali regole del gioco assumendo che 
 * tutte le condizioni necessarie al loro corretto funzionamento siano sempre verificate al mometo della chiamata.
 * 
 * @author Valerio Ferretti e Alberto Marini 
 * 
 */
public class AzioniPartita{
	
	private StrutturaDati strutturaDati;
	
	public AzioniPartita(StrutturaDati strutturaDati){
		this.strutturaDati = strutturaDati;
	}
	
	/**
	 * Funzione che distribuisce ad ogni giocatore le tessere terreno iniziali in modo tale che ognuno di loro
	 * abbia una tessera terreno differente dagli altri. 
	 * 
	 * @author Valerio Ferretti e Alberto Marini
	 */
	public void distribuisciTessereTerrenoIniziali(){
		
		List<Integer> generati = new ArrayList<Integer>(); 
		int y = 6;   
		Random rng = new Random();
		
		for (int i = 0; i < strutturaDati.getNumeroGiocatori(); i++){ 
			while(true){ 
				Integer next = rng.nextInt(y); 
				if (!generati.contains(next)){ 
					generati.add(next); 
					break; 
	            } 
	        } 
	    }
		Iterator<Integer> iteratorSequenza = generati.iterator();
		Iterator<Giocatore> iteratorGiocatori = strutturaDati.giocatori.iterator();
		
		while(iteratorSequenza.hasNext()&&iteratorGiocatori.hasNext()){
			
			int prossimoNumero = iteratorSequenza.next();
			Giocatore prossimoGiocatore = iteratorGiocatori.next();
			
			if(prossimoNumero == 0){
					prossimoGiocatore.addTesseraTerreno(new TesseraTerreno(EnumRegione.DESERTO, 0));
			}
			if(prossimoNumero == 1){
				prossimoGiocatore.addTesseraTerreno(new TesseraTerreno(EnumRegione.PIANURA, 0));
			}
			if(prossimoNumero == 2){
				prossimoGiocatore.addTesseraTerreno(new TesseraTerreno(EnumRegione.LAGO, 0));
			}
			if(prossimoNumero == 3){
				prossimoGiocatore.addTesseraTerreno(new TesseraTerreno(EnumRegione.FORESTA, 0));
			}
			if(prossimoNumero == 4){
				prossimoGiocatore.addTesseraTerreno(new TesseraTerreno(EnumRegione.COLLINA, 0));
			}
			if(prossimoNumero == 5){
				prossimoGiocatore.addTesseraTerreno(new TesseraTerreno(EnumRegione.MONTAGNA, 0));
			}
		}
	}

	/**
	 * Il metodo implementa la regola secondo la quale è possibile muovere una pacora bianca o la pecora nera
	 * da una regione adiacente alla strada in cui si trova il pastore all'altra regione confinante con la detta
	 * strada. Si assume che al momento della chiamata siano verificate tutte le condizioni che ne permettono 
	 * il corretto funzionamento. 
	 * 
	 * @param regionePartenza: è un riferimento alla regione dalla quale verra spostata la pecora. 
	 * @param stradaGiocatore: è un riferimento all'oggetto strada in cui si trova il pastore utilizzato in 
	 * questa mossa dal giocatore. 
	 * @param pecoraNera: è true se il giocatore vuole muovere la pecora nera
	 * false altrimenti
	 * 
	 * @author Valerio Ferretti e Alberto Marini
	 */
	public void muoviPecora(Regione regionePartenza, Strada stradaGiocatore, boolean pecoraNera){
		
		Regione regioneArrivo;
		
		//Calcolo regione di arrivo con info riguardo Strada in cui si trova il giocatore
		if(regionePartenza.equals(strutturaDati.g.getEdgeSource(stradaGiocatore).getId())){
			regioneArrivo  = strutturaDati.g.getEdgeTarget(stradaGiocatore);
		} else{
			regioneArrivo = strutturaDati.g.getEdgeSource(stradaGiocatore);
		}
		
		//Esecuzione spostamenti(Aggiornamento model)
		if(pecoraNera == true){
			regionePartenza.setPresenzaPecoraNera(false);
			regioneArrivo.setPresenzaPecoraNera(true);
		} else{
			regionePartenza.setNumeroPecore(regionePartenza.getNumeroPecore()-1);
			regioneArrivo.setNumeroPecore(regioneArrivo.getNumeroPecore()+1);
		}
	}
	
	/**
	 * Il metodo genera un numero casuale compreso tra 1 e 6 simulando il lancio di un dado
	 * 
	 * @author Valerio Ferretti e Alberto Marini
	 */
	public static int tiraDado(){
		
		int value;
		
		Random random = new Random();
		value = random.nextInt(6) + 1;
		return value;
	}
/**
 * Metodo che implementa la possibilita della pecora nera di muoversi all'inizio del turno di ciascun giocatore
 * in una delle regioni adiacenti alla regione in cui si trova all'invocazione del metodo. Il metodo utilizza la
 * funzione lancia dado per ottenere un numero casuale. Se il numero ottenuto è pari al valore dell'attributo numero
 * di una delle regioni adiacenti la pecora nera viene spostata altrimenti rimane do ve si trova al momento 
 * dell'invocazione. 
 * 
 * @param regionePartenza: regione in cui si trova la pecora nera al momento dell'invocazione
 * 
 * @author Valerio Ferretti e Alberto Marini
 */
	public void muoviPecoraNeraRandom(Regione regionePartenza){
		
		int value;
		Regione regioneArrivo;
		Set<Strada> insiemeStrade;
		
		value = AzioniPartita.tiraDado();
		insiemeStrade = strutturaDati.g.edgesOf(regionePartenza);
		Iterator<Strada> iterator = insiemeStrade.iterator();
		
	/*Ciclo while che scorre le strade confinanti con regione partenza*/
		while(iterator.hasNext()){
	/*Blocco if che determina se esiste e quale è la regione d'arrivo*/
			Strada strada = iterator.next();
			if(strada.getNumero() == value){
				if(regionePartenza.equals(strutturaDati.g.getEdgeSource(strada).getId())){
					regioneArrivo = strutturaDati.g.getEdgeTarget(strada);
				} else{
					regioneArrivo = strutturaDati.g.getEdgeSource(strada);
				}
	/*Esecuzione degli spostamenti (Aggiornamento del model)*/
				regionePartenza.setPresenzaPecoraNera(false);
				regioneArrivo.setPresenzaPecoraNera(true);
			}
		}
				
	}
	
	/**
	 * Metodo che implementa la possibilità di acquistare una tessera terreno. Si assume che al momento della 
	 * chiamata siano verificate tutte le condizioni che ne permettono il corretto funzionamento.
	 * 
	 * @param tipologia: tipologia della tessera terreno da acquistare
	 * @param giocatore: nome del giocatore che sta effettuando la mossa
	 * 
	 * @author Valerio Ferretti e Alberto Marini
	 */
	public void compraTesseraTerreno(EnumRegione tipologia, Giocatore giocatore){
		
		int costo;
		
		costo = strutturaDati.getCostoTesseraTerreno(tipologia);
		giocatore.setSoldi(giocatore.getSoldi() - costo);
		strutturaDati.incrementaCostoTessereTerreno(tipologia);
		giocatore.addTesseraTerreno(new TesseraTerreno(tipologia,costo));
	}

	/**
	 * Metodo che implementa la possibilità di muovere il pastore da una regione all'altra. Se lo spostamento è
	 * tra strade adiacenti è gratuito altrimenti costa 1 moneta. Si assume che al momento della chiamata siano 
	 * verificate tutte le condizioni che ne permettono il corretto funzionamento.
	 * 
	 * @param stradaPartenza: strada occupata dal pastore al mometo della chiamata del metodo
	 * @param stradaArrivo: strada segnalata dall'utente come strada come strada che sulla quale si deve spostare
	 * pastore
	 * @param giocatore: attributo nome del giocatore che richiede la mossa
	 * @param i: pastore associato al giocatore. Puo avere 2 valori, 0 e 1. Nel caso in cui giacno piu di due
	 * giocatori ad ogni giocatore è associata una e una sola posizione e i vale 0. Nel cosa i giocatori siano due
	 * ad ogni giocatore sono associati due pastori; il primo utilizzato se i = 0 il secondo se i = 1.
	 * 
	 *  @author Valerio Ferretti e Alberto Marini
	 */
	public void muoviPastore(Strada stradaPartenza, Strada stradaArrivo, Giocatore giocatore, int i){
		
		Regione regioneASPartenza1;
		Regione regioneASPartenza2;
		Regione regioneASArrivo1;
		Regione regioneASArrivo2;		
		
		regioneASPartenza1 = strutturaDati.g.getEdgeSource(stradaPartenza);
		regioneASPartenza2 = strutturaDati.g.getEdgeTarget(stradaPartenza);
		regioneASArrivo1 = strutturaDati.g.getEdgeSource(stradaArrivo);
		regioneASArrivo2 = strutturaDati.g.getEdgeTarget(stradaArrivo);
		
		
		stradaPartenza.setStato(EnumStrada.LIBERA);
		stradaArrivo.setStato(EnumStrada.O_PASTORE);
		giocatore.setPosizione(i, stradaArrivo.getId());
		
		if(!(regioneASPartenza1.equals(regioneASArrivo1.getId())||regioneASPartenza1.equals(regioneASArrivo2.getId())||
				regioneASPartenza2.equals(regioneASArrivo1.getId())||regioneASPartenza2.equals(regioneASArrivo2.getId()))){
			giocatore.setSoldi(giocatore.getSoldi()-1);
		}
	/*Posizione i recinti finale e non nella casella lasciata libera dal pastore*/
		if(strutturaDati.getDisponibilitàRecinto() > 0){
			stradaPartenza.setStato(EnumStrada.O_RECINTO);
			strutturaDati.decrementaDisponibilitàRecinto();
		} else if(strutturaDati.getDisponibilitaRecintoFinale() > 0){
			stradaPartenza.setStato(EnumStrada.O_RECINTOFINALE);
			strutturaDati.decrementaDisponibilitaRecintoFinale();
		}
 	}
	
	/**
	 * Funzione che calcola, al termine della partita, il guadagno dei giocatori e aggiorna il campo soldi di ogni
	 * oggetto giocatore
	 * 
	 * @author Valerio Ferretti e e Alberto Marini	
	 */
	public void calcolaPunteggioFinale(){
		
		Giocatore giocatore;
		int numeroPecore[] = new int[6];
		int guadagnoGiocatore = 0;
		
		Iterator<Regione> iteratorRegioni = strutturaDati.regioni.iterator();
		while(iteratorRegioni.hasNext()){
			Regione regione = iteratorRegioni.next();
			if(regione.getTipologia() == EnumRegione.DESERTO){
				numeroPecore[0] = numeroPecore[0] + (regione.getNumeroPecore());
			}
			if(regione.getTipologia() == EnumRegione.PIANURA){
				numeroPecore[1] = numeroPecore[1] + (regione.getNumeroPecore());
			}
			if(regione.getTipologia() == EnumRegione.LAGO){
				numeroPecore[2] = numeroPecore[2] + (regione.getNumeroPecore());
			}
			if(regione.getTipologia() == EnumRegione.FORESTA){
				numeroPecore[3] = numeroPecore[3] + (regione.getNumeroPecore());
			}
			if(regione.getTipologia() == EnumRegione.COLLINA){
				numeroPecore[4] = numeroPecore[4] + (regione.getNumeroPecore());
			}
			if(regione.getTipologia() == EnumRegione.MONTAGNA){
				numeroPecore[5] = numeroPecore[5] + (regione.getNumeroPecore());
			}
		}
		
		Iterator<Giocatore> iteratorGiocatori = strutturaDati.giocatori.iterator();
		while(iteratorGiocatori.hasNext()){
			giocatore = iteratorGiocatori.next();
						
			guadagnoGiocatore = guadagnoGiocatore + (giocatore.numeroDeserti()*numeroPecore[0]);
			guadagnoGiocatore = guadagnoGiocatore + (giocatore.numeroPianure()*numeroPecore[1]);
			guadagnoGiocatore = guadagnoGiocatore + (giocatore.numeroLaghi()*numeroPecore[2]);
			guadagnoGiocatore = guadagnoGiocatore + (giocatore.numeroForeste()*numeroPecore[3]);
			guadagnoGiocatore = guadagnoGiocatore + (giocatore.numeroColline()*numeroPecore[4]);
			guadagnoGiocatore = guadagnoGiocatore + (giocatore.numeroMontagne()*numeroPecore[5]);
								
			giocatore.setSoldi(giocatore.getSoldi() + guadagnoGiocatore);
			guadagnoGiocatore = 0;
		}		
	}		
	
}