package it.polimi.ferretti_marini.controller;

import it.polimi.ferretti_marini.model.*;
import java.util.*;

/**
 * Classe che contine dei metodi il cui obbiettivo è verificare che siano verificate le condizioni tali per cui
 * i metodi della classe AzioniPartita posso essere eseguiti correttamente. 
 * 
 * @author Valerio
 *
 */
public class ControlloAzioniPartita {

	StrutturaDati strutturaDati;
	AzioniPartita azioniPartita;
	
	public ControlloAzioniPartita(StrutturaDati strutturaDati, AzioniPartita azioniPartita){
		this.strutturaDati = strutturaDati;
		this.azioniPartita = azioniPartita;
	}
	
	/**
	 * Metodo il cui obbiettivo è verificare che siano verificate tutte le precondizioni del metodo muoviPecora 
	 * della classe AzioniPartita. Verifica inoltre che la mossa muoviPecora sia disponibile, ovvero rispetti
	 * i vincoli imposti sulle mosse che un giocatore puo e non puo effetture in un solo turno. Verificate tutte
	 * le condizioni richiama la mossa muoviPecora() di AzioniPartita
	 * 
	 * @param regionePartenza: regione dalla quali si vuole spostare una pecora
	 * @param giocatore: campo nome del giocatore che richiede la mossa
	 * @param pecoraNera: true se si vuole spostare la pecora nera, false altrimenti
	 * @param i: 0 se si vuole selezionare il primo pastore associato al giocatore, 1 se si vuole selezionare 
	 * il secondo(solo in caso di due giocatori)
	 * 
	 * @return true se tutte le condizioni sono verificate ed è stato richiamto il metodo muoviPecora, false 
	 * altrimenti
	 * 
	 * @author Valerio Ferretti e Alberto Marini
	 */
	public boolean controlloMuoviPecora(Regione regionePartenza, Giocatore giocatore, boolean pecoraNera, int i){
		
		int idStrada;
		boolean condizione1 = false;
		boolean condizione2 = false;
		boolean condizione3 = false;
		Strada stradaGiocatore = new Strada();
		
		/*Verifico che la strada in cui si trova il giocatore sia adiacente a regione partenza e ottengo un 
		riferimento a tale strada. In caso di condizione verificata condizione1 = true*/
		idStrada = giocatore.getPosizione(i);
		Set<Strada> insiemeStrade;
		insiemeStrade = strutturaDati.g.edgesOf(regionePartenza);
		Iterator<Strada> iteratorStrade = insiemeStrade.iterator();
		while(iteratorStrade.hasNext()){
			Strada strada = iteratorStrade.next();
			if(strada.equals(idStrada)){
				stradaGiocatore = strada;
				condizione1 = true;
			} else{
				stradaGiocatore = null;
			}
		}
		
		/*Verifico che in regionePartenza vi sia un numero di pecore maggiore stretto di zero. Se condizione 
		  verificata condizione2 = true*/
		if(regionePartenza.getNumeroPecore()>0){
			condizione2 = true;
		}
		
		/*Verifico che la mossa muoviPastore sia disponibile. Se condizione verificata condizione3 = true*/
		int somma = 0;
		for(int j=0; j<3; j++){
			somma = somma + strutturaDati.disponibiltaMosse[i];
		}
		
		if(!((strutturaDati.disponibiltaMosse[0] == 1 && strutturaDati.disponibiltaMosse[1] == 2)||
				(strutturaDati.disponibiltaMosse[0] == 0 && strutturaDati.disponibiltaMosse[1] == 1)||
				(somma == 3 && strutturaDati.disponibiltaMosse[0] == 0))){
			condizione3 =true;
		}
		
		/*Aggiornamento struttura dati ed esecuzione del movimento*/
		if(condizione1&&condizione2&&condizione3){
			if(somma == 0){
				strutturaDati.disponibiltaMosse[1] = 1;
			}
			if(somma == 1){
				strutturaDati.disponibiltaMosse[1] = 2;
			}
			if(somma == 3){
				strutturaDati.disponibiltaMosse[1] = 3;
			}
			azioniPartita.muoviPecora(regionePartenza, stradaGiocatore, pecoraNera);
			return true;
		} else{
			System.out.println("La mossa non è disponibile");
			return false;
		}	
	}

	/**
	 * Metodo il cui obbiettivo è verificare che siano verificate tutte le precondizioni del metodo 
	 * compraTesseraTerreno della classe AzioniPartita. Verifica inoltre che la mossa compraTesseraTerreno sia 
	 * disponibile ovvero rispetti i vincoli imposti sulle mosse che un giocatore puo e non puo effetture in un 
	 * solo turno. Verificate tutte le condizioni richiama la mossa compraTesseraTerreno() di AzioniPartita.
	 * 
	 * @param tipologia: tipologia della tessera terreno da acquistare
	 * @param giocatore: campo nome del giocatore che richiede la mossa
	 * 
	 * @return: true se tutte le condizioni sono verificate ed è stato richiamto il metodo muoviPecora, false 
	 * altrimenti
	 * 
	 * @author Valerio Ferretti e Alberto Marini
	 */
	public boolean controlloCompraTessereTerreno(EnumRegione tipologia, Giocatore giocatore){
		
		boolean condizione1 = false;
		boolean condizione2 = false;
		
		/*Verfico che vi siano ancora TessereTerreno disponibili del tipo tipologia. Se la condizione è verificata
		  condizione1 = true*/
		if(strutturaDati.getCostoTesseraTerreno(tipologia) == 5){
			System.out.println("Nessuna tessera terreno di questo tipo è disponibile");
		} else{
			condizione1 = true;
		}
		
		/*Verifico che la mossa compraTessereTerreno sia disponibile. Se condizione verificata condizione3 = true*/
		int somma = 0;
		for(int i=0; i<3; i++){
			somma = somma + strutturaDati.disponibiltaMosse[i];
		}
		
		if(!((strutturaDati.disponibiltaMosse[0] == 1 && strutturaDati.disponibiltaMosse[2] == 2)||
				(strutturaDati.disponibiltaMosse[0] == 0 && strutturaDati.disponibiltaMosse[2] == 1)||
				(somma == 3 && strutturaDati.disponibiltaMosse[0] == 0))){
			condizione2 =true;
		}
		
		/*Aggiornamento struttura dati ed esecuzione del movimento*/
		if(condizione1&&condizione2){
			if(somma == 0){
				strutturaDati.disponibiltaMosse[2] = 1;
			}
			if(somma == 1){
				strutturaDati.disponibiltaMosse[2] = 2;
			}
			if(somma == 3){
				strutturaDati.disponibiltaMosse[2] = 3;
			}
			azioniPartita.compraTesseraTerreno(tipologia, giocatore);
			return true;
		} else{
			System.out.println("La mossa non è disponibile");
			return false;
		}	
	}

	/**
	 * Metodo il cui obbiettivo è verificare che siano verificate tutte le precondizioni del metodo muoviPastore 
	 * della classe AzioniPartita. Verifica inoltre che la mossa muoviPastore sia disponibile ovvero rispetti i 
	 * vincoli imposti sulle mosse che un giocatore puo e non puo effetture in un solo turno. Verificate tutte le
	 * condizioni richiama la mossa muoviPastore() di AzioniPartita.
	 * 
	 * @param stradaPartenza: strada in cui si trova il pastore al momento della chiamata
	 * @param stradaArrivo: strada in cui dovra essersi posizionato il pastore al termine dell'esecuzione del metodo
	 * @param giocatore: campo nome del giocatore che richiede la mossa
	 * @param i: 0 se si vuole selezionare il primo pastore associato al giocatore, 1 se si vuole selezionare 
	 * il secondo(solo in caso di due giocatori)
	 * 
	 * @return: true se tutte le condizioni sono verificate ed è stato richiamto il metodo muoviPecora, false 
	 * altrimenti
	 * 
	 * @author Valerio Ferretti e Alberto Marini
	 */
	public boolean controllaMuoviPastore(Strada stradaPartenza, Strada stradaArrivo, Giocatore giocatore, int i){
		
		boolean condizione1 = false;
		boolean condizione2 = false;
		boolean condizione3 = false;
		boolean condizione4 = false;
		
		/*Verifico che la posizione del pastore scelto sia coincidente con stradaPartenza */
		if(giocatore.getPosizione(i) == stradaPartenza.getId()){
			condizione4 = true;
		}
		
		/*Verifico che la strada di arrivo sia libera. Se la condizione è verificata condizione1 = true*/
		if(stradaArrivo.getStato() == EnumStrada.LIBERA){
			condizione1 = true;
		}
		
		/*Verifico che nella strada di partenza ci sia il pastore. Se la condizione è verificata condizione2 = true*/
		if(stradaPartenza.getStato() == EnumStrada.O_PASTORE){
			condizione2 = true;
		}
		
		/*Verifico che ci sia almeno un recinto finale ancora disponibile. Se la condizione è verificata condizione3 = true*/
		if(strutturaDati.getDisponibilitaRecintoFinale() > 0){
			condizione3 = true;
		}
		
		/*Controllo condizione ed esecuzione operazioni*/
		int somma = 0;
		for(int k=0; k<3; k++){
			somma = somma + strutturaDati.disponibiltaMosse[k];
		}
		
		if(condizione1&&condizione2&&condizione3&&condizione4){
			if(somma == 0){
				strutturaDati.disponibiltaMosse[0] = 1;
			}
			if(somma == 1){
				strutturaDati.disponibiltaMosse[0] = 2;
			}
			if(somma == 3){
				strutturaDati.disponibiltaMosse[0] = 3;
			}
			azioniPartita.muoviPastore(stradaPartenza, stradaArrivo, giocatore, i);	
			return true;
		} else{
			System.out.println("La mossa non è disponibile");
			return false;
		}		
	}
	
}