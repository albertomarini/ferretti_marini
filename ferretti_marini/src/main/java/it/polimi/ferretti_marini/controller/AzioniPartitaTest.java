package it.polimi.ferretti_marini.controller;

import static org.junit.Assert.*;
import java.io.IOException;
import java.util.Iterator;
import org.jdom2.JDOMException;
import org.junit.Before;
import org.junit.Test;
import it.polimi.ferretti_marini.model.*;
import it.polimi.ferretti_marini.Application.*;
import it.polimi.ferretti_marini.Rete.*;

public class AzioniPartitaTest {
	
	private StrutturaDati strutturaDati;
	private AzioniPartita azioniPartita;
	
	@Before
	public void setUp() throws JDOMException, IOException{
		
		/*Inizializzazione strutture dati necessarie al test di AzioniPartita*/
		strutturaDati = new StrutturaDati();
		strutturaDati.addGiocatore("Valerio", EnumColore.ROSSO, 25, 5, 15);
		strutturaDati.addGiocatore("Alberto", EnumColore.GIALLO, 2, 9, 5);
		azioniPartita = new AzioniPartita(strutturaDati);
		strutturaDati.xmlToStrade();
		strutturaDati.xmlToRegioni();
		strutturaDati.xmlToGrafo();
	}
	
	@Test
	public void haDistribuitoTessereTerrenoIniziali(){
		
		Iterator<Giocatore> iteratorGiocatori = strutturaDati.giocatori.iterator();
		azioniPartita.distribuisciTessereTerrenoIniziali();
		
		for(int i = 0; i < strutturaDati.getNumeroGiocatori(); i ++){
			Giocatore giocatore = iteratorGiocatori.next();
						
			assertTrue("Le tessere terreno iniziali non sono state distribuite correttamente", 
					giocatore.numeroColline() != 0 || giocatore.numeroDeserti() != 0 || giocatore.numeroForeste() != 0 ||
					giocatore.numeroLaghi() != 0 || giocatore.numeroMontagne() != 0 || giocatore.numeroPianure() != 0);
		}
	}
	
	@Test
	public void haCompratoTesseraTerreno(){
		
		Iterator<Giocatore> iteratorGiocatori = strutturaDati.giocatori.iterator();
		
		Giocatore giocatore1 = iteratorGiocatori.next();
		azioniPartita.compraTesseraTerreno(EnumRegione.COLLINA, giocatore1);
		Giocatore giocatore2 = iteratorGiocatori.next();
		azioniPartita.compraTesseraTerreno(EnumRegione.COLLINA, giocatore2);
		
		assertTrue("La tesseraTerreno collina non è stata comprata correttamente dal giocatore1", giocatore1.numeroColline() == 1);
		assertTrue("Il campo soldi non è stato aggiornato correttamente", giocatore1.getSoldi() == 15);
		
		assertTrue("La tesseraTerreno collina non è stata comprata correttamente dal giocatore2", giocatore1.numeroColline() == 1);
		assertTrue("Il campo soldi non è stato aggiornato correttamente", giocatore2.getSoldi() == 4);
	}
	
	/*@Test
	public void hoMossoLaPecora(){
		
		Regione regionePartenza = null;
		Regione regioneArrivo = null;
		Strada stradaGiocatore = null;
		
		Iterator<Regione> iteratorRegioni = strutturaDati.regioni.iterator();
		while(iteratorRegioni.hasNext()){
			regionePartenza = iteratorRegioni.next();
			if(regionePartenza.equals(2)){
				break;
			}
		}
		Iterator<Strada> iteratorStrade = strutturaDati.strade.iterator();
		while(iteratorStrade.hasNext()){
			stradaGiocatore = iteratorStrade.next();
			if(stradaGiocatore.equals(25)){
				break;
			}
		}
		if(regionePartenza.equals(strutturaDati.g.getEdgeSource(stradaGiocatore).getId())){
			regioneArrivo  = strutturaDati.g.getEdgeTarget(stradaGiocatore);
		}
		else{
			regioneArrivo = strutturaDati.g.getEdgeSource(stradaGiocatore);
		}
		
		azioniPartita.muoviPecora(regionePartenza, stradaGiocatore, false);
		
		assertTrue("In regionePartenza non è stata spostata la pecora", regionePartenza.getNumeroPecore() == 0);
		assertTrue("Nella regione adiacenta a stradaGiocatore nonè stata aggiunta una pecora", 
				regioneArrivo.getNumeroPecore() == 2);
	}*/
}