package it.polimi.ferretti_marini.Application;

import java.io.IOException;
import it.polimi.ferretti_marini.Rete.*;

/**
 * Classe contenente un solo metodo che implementa il flusso del gioco lato client dalla sua inializzazione, 
 * alla gestione dei turni fino alla determinazione del vincitore. 
 * 
 * @author Valerio Ferretti e Alberto Marini
 *
 */
public class ApplicationClient{
	
	ClientSocket clientSocket;
	int port;
	String ip;
	
	public ApplicationClient(){
		
		clientSocket = new ClientSocket("127.0.0.1", 1337);
	}
	
	/**
	 * Implementa il flusso del gioco lato client dalla sua inializzazione, alla gestione dei turni fino alla 
	 * determinazione del vincitore richiamando funzioni definite nella classe ApplicationClient. Dialoga tramite i 
	 * socket con la funzione server inviando e ricevendo messaggi tramite stringhe.  
	 * 
	 *  @author Valerio Ferretti e Alberto Marini
	 */
	public static void client() {
		
		ApplicationClient applicationClient = new ApplicationClient();
		System.out.println("Benvenuti nel gioco di sheepland!");
				
		try {
			applicationClient.clientSocket.inizializzaConnessioneSocketClient();
		} catch (IOException e) {
			e.printStackTrace();
		}
		applicationClient.clientSocket.inizzializzaGiocatore();
		
		System.out.println("Cominciamo a giocare ...\n\n");
		
		applicationClient.clientSocket.esecuzioneTurnoUtenteSocket();
		
		/*Ricevi dal server iformazione riguardo vincitore/perdente e termina il gioco*/
		applicationClient.clientSocket.stampaVincitore();
	}

}