package it.polimi.ferretti_marini.Application;

import java.io.IOException;

import org.jdom2.JDOMException;

import it.polimi.ferretti_marini.model.*;
import it.polimi.ferretti_marini.controller.*;
import it.polimi.ferretti_marini.Rete.*;

/**
 * Classe contenente un solo metodo che implementa il flusso del gioco lato server dalla sua inializzazione,
 *  alla gestione dei turni fino alla determinazione del vincitore. 
 * 
 * @author Valerio Ferretti e Alberto Marini
 *
 */
public class ApplicationServer{
	
	int port;
	StrutturaDati strutturaDati;
	AzioniPartita azioniPartita;
	ControlloAzioniPartita controlloAzioniPartita;
	Server_Socket server_Socket;
	
	public ApplicationServer(int port){
		
		strutturaDati = new StrutturaDati();
		azioniPartita = new AzioniPartita(strutturaDati);
		controlloAzioniPartita = new ControlloAzioniPartita(strutturaDati, azioniPartita);
		server_Socket = new Server_Socket(port, strutturaDati, azioniPartita, controlloAzioniPartita);
	}

	/**
	 * Implementa il flusso del gioco lato server dalla sua inializzazione, alla gestione dei turni fino alla 
	 * determinazione del vincitore richiamando funzioni definite nelle classi StrutturaDati, Server_Socket e 
	 * ApplicationServer. Dialoga tramite i socket con la funzione client inviando e ricevendo messaggi tramite 
	 * stringhe.  
	 * 
	 * @throws JDOMException: eccezzione che puo essere sollevata dai metodi xmlToStrade(), xmlToRegioni(), xmlToGrafo() in 
	 * seguito di un errore nel parsing delle xml ai valori utilizzati per inizializzare le strutture dati.
	 * @throws IOException: DOMException: eccezzione che puo essere sollevata dai metodi xmlToStrade, xmlToRegioni, xmlToGrafo in 
	 * seguito di un errore nel parsing delle xml ai valori utilizzati per inizializzare le strutture dati.
	 * 
	 * @author Valerio Ferretti e Alberto Marini
	 */
	public static void server() throws JDOMException, IOException {
	
		System.out.println("Benvenuti nel gioco di sheepland!");
		ApplicationServer applicationServer = new ApplicationServer(1337);
		
		applicationServer.strutturaDati.xmlToStrade();
		applicationServer.strutturaDati.xmlToRegioni();
		applicationServer.strutturaDati.xmlToGrafo();
		System.out.println("La strade, le regioni e la mappa sono state istanziate!");
	
		try {
			applicationServer.server_Socket.inizializzaConnessioniSocketServer();
		} catch (IOException e) {
			e.printStackTrace();
		}
		applicationServer.server_Socket.inizializzaGiocatoriSocket();
				
		System.out.println("Cominciamo a giocare!");
		
		applicationServer.server_Socket.escuzioneTurnoUtenteSocket();
		
		applicationServer.server_Socket.calcolaVinciore();	
	}
	
}