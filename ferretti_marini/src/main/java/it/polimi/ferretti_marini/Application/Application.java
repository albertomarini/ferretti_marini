package it.polimi.ferretti_marini.Application;


import java.io.IOException;
import java.util.Scanner;

import org.jdom2.JDOMException;

public class Application {

	/**
	 * Avvia l'applicazione richiedendo se essa è da avviare come cliant o come server. 
	 * 
	 * @throws JDOMException: dichiarazione dell apossibilità che server() di ApllicationServer o client() di
	 * ApplicationClient sollevino JDOMException
	 * @throws IOException: dichiarazione dell apossibilità che server() di ApllicationServer o client() di
	 * ApplicationClient sollevino IOException
	 */
	public static void main(String[] args) throws JDOMException, IOException {

		System.out.println("Digita 0 per avviare il server, 1 per avviare il client");
		while(true){
			Scanner in = new Scanner(System.in);
			int scelta = in.nextInt();
			if(scelta == 0){
				ApplicationServer.server();
			}else if(scelta == 1){
				ApplicationClient.client();
			}else if(scelta == 2){
				break;
			}else if(scelta != 1 && scelta !=2 && scelta !=0){
				System.out.println("Digita un valore corretto");
			}
		}
	}
}