package it.polimi.ferretti_marini.Rete;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Iterator;
import java.util.Random;
import java.util.Scanner;

import it.polimi.ferretti_marini.controller.AzioniPartita;
import it.polimi.ferretti_marini.controller.ControlloAzioniPartita;
import it.polimi.ferretti_marini.model.*;

/**
 * Classe che implementa tutte quelle funzioni richiamate dalla classe ApplicationServer per rendere il flusso
 * della partita. Contiene istanze strutturaDati, azioniPartita e controlloAzioniPartita per richiamarne i metodi
 * e le struttureDati per contenere le variabili Socket, Scanner e PrintWriter per scambiarsi informazioni tramite
 * socket. 
 * 
 * @author Valerio Ferretti e Alberto Marini
 *
 */
public class Server_Socket {

	private StrutturaDati strutturaDati;
	private AzioniPartita azioniPartita;
	private ControlloAzioniPartita controlloAzioniPartita;
	int port;
	private Socket[] socketVettore = {null, null, null, null, null};
	private Scanner[] socketIn = {null, null, null, null};
	private PrintWriter[] socketOut = {null, null, null, null, null};
	private boolean fineGioco = false;


	public Server_Socket(int port, StrutturaDati strutturaDati, AzioniPartita azioniPartita, ControlloAzioniPartita controlloAzioniPartita){
		this.port = port;
		this.strutturaDati = strutturaDati;
		this.azioniPartita = azioniPartita;
		this.controlloAzioniPartita = controlloAzioniPartita;
	}

	/**
	 * Inizilizza la prima e la seconda connessione con il client dopo di che chiede al secondo client e ai 
	 * successivi con un una stringa tramite se attendere un'altro giocatore o iniziare a giocare. Al termine 
	 * esecuzione i tre vettori socketVettore, socketIn, socketOut contengono rispettivamente i socket e le 
	 * variabili di tipo Scanner e PrintWriter per comunicare con i client.
	 * 
	 * @author Valerio Ferretti e Alberto Marini
	 */
	public void inizializzaConnessioniSocketServer() throws IOException{
		
		ServerSocket serverSocket;
		int j = 0;
		int k = 0;
		 		
		System.out.println("Creazione di un ServerSocket");
		serverSocket = new ServerSocket(port);
		
		for(int i = 0; i < 4; i++){
			
			/*Connessione stabilita*/
			socketVettore[i] = serverSocket.accept();
			System.out.println("Connessione accettata!");
			strutturaDati.incrementaNumeroGiocatori();
			
			/*Creazioni varibili per le connessioni e notifica al client*/
			socketIn[i] = new Scanner(socketVettore[i].getInputStream());
			socketOut[i] = new PrintWriter(socketVettore[i].getOutputStream());
			socketOut[i].println("Connessione accettata!");
			socketOut[i].flush();
								
			/*Modulo che invia all'ultimo client creato il numero di giocatori finora connessi per consentire al
			  client creato di sapere se deve rispondere ad una richiesta eventuale del server (if successivo)*/
			while(socketVettore[j] != null){
				j = j+1;
			}
			socketOut[j-1].println(strutturaDati.getNumeroGiocatori());
			socketOut[j-1].flush();

			/*Se i giocatori sono maggiori o uguali a dare la possibilita di interrompere il ciclo e iniziare a giocare*/
			if(strutturaDati.getNumeroGiocatori() > 1){
				if(i == 3){
					break;
				}
				socketOut[i].println("Digita 'STOP' se vuoi iniziare a giocare 'CONTINUE' se vuoi attendere nuovi giocatori");
				socketOut[i].flush();
				String scelta = socketIn[i].nextLine();
				System.out.printf("Il comando digitato è %s\n", scelta);
				if(scelta.equals("STOP")){
					break;
				}
			}
			if(strutturaDati.getNumeroGiocatori() < 4){
				System.out.println("Attesa di un nuovo Giocatore\n");
			}
		}
		
		/*Invio ai client informazione riguardante il numero dei giocatori*/
		while(socketOut[k] != null){
			socketOut[k].println(strutturaDati.getNumeroGiocatori());
			socketOut[k].flush();
			k = k + 1;
		}
	serverSocket.close();
	}
	
	/**
	 * Metodo che richiede agli utenti di cui sono state acettate le connessioni i parametri neccessari per 
	 * inizilizzare gli oggetti di tipo Giocatore rappresentanti i giocatori. Una volta inizializzato l'oggeto il
	 * metodo lo aggiunge all'arrayList giocatori.
	 * 
	 * @author Valerio Ferretti e Alberto Marini 
	 */
	public void inizializzaGiocatoriSocket(){
		
		int soldi;
		int posizione1 = 0;
		int posizione2 = 0;
	
		System.out.printf("Il numero di giocotori è: %d\n", strutturaDati.getNumeroGiocatori());
		
		for(int i = 0; i < strutturaDati.getNumeroGiocatori(); i++){
		
			/*Acquisizione informazioni nome e colore scelto*/
			socketOut[i].println("Inserisci Colore(Maiuscolo) e Nome del giocatore");
			socketOut[i].flush();
			EnumColore colore = EnumColore.valueOf(socketIn[i].nextLine());
			String nome = socketIn[i].nextLine();
			socketOut[i].println("Informazioni nome e colore registrate");
			socketOut[i].flush();
			System.out.println("Informazioni nome e colore registrate");

			
			/*Aquisizione informazioni soldi*/
			if(strutturaDati.getNumeroGiocatori() == 2){
				soldi = 30;
			}else{
				soldi = 20;
			}
			socketOut[i].println("Soldi inizializzati");
			socketOut[i].flush();
			System.out.println("Soldi inizializzati");			
			
			/*Aquisizione informazioni posizione*/
			Iterator<Strada> iteratorStrade1 = strutturaDati.strade.iterator();
	
			Random random1 = new Random();
			posizione1 = random1.nextInt(42);
			while(iteratorStrade1.hasNext()){
				Strada strada1 = iteratorStrade1.next();
				if(strada1.equals(posizione1) && strada1.getStato() == EnumStrada.LIBERA){
					strada1.setStato(EnumStrada.O_PASTORE);
					System.out.printf("La posizione 0 del giocatore %d è: %d\n", i, posizione1);
					break;
				}
			}
		
			if(strutturaDati.getNumeroGiocatori() == 2){
				Iterator<Strada> iteratorStrade2 = strutturaDati.strade.iterator();
				Random random2 = new Random();
				posizione2 = random2.nextInt(42);
				while(iteratorStrade2.hasNext()){
					Strada strada2 = iteratorStrade2.next();
					if(strada2.equals(posizione2) && strada2.getStato() == EnumStrada.LIBERA){
						strada2.setStato(EnumStrada.O_PASTORE);
						System.out.printf("La posizione 1 del giocatore %d è: %d\n", i, posizione2);
						break;
					}
				}
			} else{
				posizione2 = 0;
			}
			
			socketOut[i].println("Posizioni inizializzate");
			socketOut[i].flush();
			System.out.println("Posizioni inizializzate");

			strutturaDati.addGiocatore(nome, colore, posizione1, posizione2, soldi);
				
			System.out.println("L'oggetto giocatore è stato creato ed aggiunto alle opportune strutture dati");	
			socketOut[i].println("L'oggetto giocatore è stato creato ed aggiunto alle opportune strutture dati");	
			socketOut[i].flush();
		}
		azioniPartita.distribuisciTessereTerrenoIniziali();
		
		for(int i = 0; i < strutturaDati.getNumeroGiocatori(); i++){
			socketOut[i].println("Tessera terreno iniziale aggiunta");
			socketOut[i].flush();
		}
	}
	
	/**
	 * Metodo che gestisce i turni degli utenti continua a richiedere ai giocatori connessi uno dopo l'altro 
	 * di scegliere le loro tre mosse fino a che non terminano i recinti finali. Attraverso un while richiede di 
	 * scegliere una determinata mossa finche essa non è valida. Attraverso un for richiede che l'utente scelga
	 * tre mosse valide per turno e cempre attraverso un for ripete tutto cio per tutti i giocatori connessi fino
	 * alla fine dei recinti finali. 
	 * 
	 * @author Valerio Ferretti e Alberto Marini
	 */
	public void escuzioneTurnoUtenteSocket(){
		
		boolean mossaEseguita;
		int mossaRichiesta = 3;
		
		/*Ciclo for di lunghezza pari al numero dei giocatori che consente a tutti i client di eseguire il 
		loro turno*/
		for(int i = 0; i < strutturaDati.getNumeroGiocatori(); i++){
				
			/*Ciclo for che consente ad ogni utente di eseguire tutte le sue tre mosse*/
			for(int j = 0; j < 3; j++){
				
				mossaEseguita = false;

				/*Ciclo while che consente all'utente di ripetere la mossa finche questa non è valida*/
				while(!mossaEseguita){
					
					socketOut[i].println("Scegli la mossa da eseguire");
					socketOut[i].flush();
					socketOut[i].println("0-->muoviPecora, 1-->compraTesseraTerreno, 2-->muoviPastore");
					socketOut[i].flush();
					
					/*Traduzione intero digitato dal client nella mossa da affettuare e richiesta all'utente 
					dei parametri necessarci a richiamare le funzioni di controllo*/
					mossaRichiesta = Integer.parseInt(socketIn[i].nextLine());

					if(mossaRichiesta == 0){
						socketOut[i].println("Hai richesto la mossa muoviPecora!");
						socketOut[i].flush();
						/*Invocazione funzione che riceve i parametri mossa e invoca la funzione di controllo
						di quella specifica mossa. Tale funzione ritorna il medesimo valore di verita ritornato
						dalla funzione di controllo. Con questo valore viene impostata mossaEseguita per poter
						uscire dal ciclo.*/
						
						mossaEseguita = richiediParametriMuoviPecora(i);
						System.out.printf("Il valore della variabile mossaEseguita è %b", mossaEseguita);

					} else if(mossaRichiesta == 1){
						socketOut[i].println("Hai richesto la mossa compraTesseraTerreno!");
						socketOut[i].flush();
						
						mossaEseguita = richiediParametriCompraTesseraTerreno(i);
						System.out.printf("Il valore della variabile mossaEseguita è %b", mossaEseguita);
						
					} else if(mossaRichiesta == 2){
						socketOut[i].println("Hai richesto la mossa muoviPastore!");
						socketOut[i].flush();
						if(strutturaDati.getDisponibilitaRecintoFinale() == 0){
							fineGioco = true;
						}
						
						mossaEseguita = richiediParametriMuoviPastore(i);
						System.out.printf("Il valore della variabile mossaEseguita è %b\n", mossaEseguita);
					}
					
					/*Invio al client del valore di mossa eseguita*/
					socketOut[i].println(mossaEseguita);
					
					if(mossaEseguita){
						socketOut[i].println("Mossa eseguita con successo!!\n\n\n");
						socketOut[i].flush();
					} else{
						socketOut[i].println("La mossa non è stata eseguita!");
						socketOut[i].flush();
					}
				}
				
				for(int s = 0; s < strutturaDati.getNumeroGiocatori(); s++){
					socketOut[s].println(fineGioco);
				}
				if(fineGioco){
					System.out.println("I recinti finali sono terminati. Fine Gioco!");
				}
			}
		}
	}
	
	/**
	 * Funzione che richiede i paramtri per eseguire il metodo controllaMuovPecora() che a sua volta richiamarà
	 * in caso di verifica corretta delle precondizioni il metodo muoviPecora() di AzioniPartita.
	 * 
	 * @param i: indice del vettore socketVettore contenente l'oggetto Socket per comunicare con il client corretto
	 * @return: true se controllaMuoviPecora() ritorna true (mossa eseguita), false altrimenti (la mossa non puo
	 * essere eseguita)
	 * 
	 *  @author Valerio Ferretti e Alberto Marini
	 */
	public boolean richiediParametriMuoviPecora(int i){
		
		Giocatore giocatore = null;
		Regione regionePartenza = null;
		
		socketOut[i].println("Inserisci nell'ordine i seguenti parametri:");
		socketOut[i].flush();
		socketOut[i].println("Id_Regione_Partenza, Nome_Utente, boolean Pecora_Nera");
		socketOut[i].flush();
		
		int id_Regione_Partenza = Integer.parseInt(socketIn[i].nextLine());
		String Nome_Utente = socketIn[i].nextLine();
		boolean pecora_Nera = Boolean.valueOf(socketIn[i].nextLine());
		int posizione = Integer.parseInt(socketIn[i].nextLine());
		
		Iterator<Regione> iteratorRegioni = strutturaDati.regioni.iterator();
		while(iteratorRegioni.hasNext()){
			regionePartenza = iteratorRegioni.next();
			if(regionePartenza.getId() == id_Regione_Partenza){
				break;
			}
		}
		
		Iterator<Giocatore> iteratorGiocatori = strutturaDati.giocatori.iterator();
		while(iteratorGiocatori.hasNext()){
			giocatore = iteratorGiocatori.next();
			if(giocatore.getNome() == Nome_Utente){
				break;
			}
		}

		return controlloAzioniPartita.controlloMuoviPecora(regionePartenza, giocatore, pecora_Nera, posizione);
	}
	
	/**
	 * Funzione che richiede i paramtri per eseguire il metodo controlloCompraTesseraTerreno()() che a sua volta 
	 * richiamarà in caso di verifica corretta delle precondizioni il metodo compraTesseraTerreno() di 
	 * AzioniPartita.
	 * 
	 * @param i: indice del vettore socketVettore contenente l'oggetto Socket per comunicare con il client corretto
	 * @return: true se controllaCompraTesseraTerreno() ritorna true (mossa eseguita), false altrimenti 
	 * (la mossa non puo essere eseguita)
	 * 
	 *  @author Valerio Ferretti e Alberto Marini
	 */
	public boolean richiediParametriCompraTesseraTerreno(int i){
		
		Giocatore giocatore = null;
		
		socketOut[i].println("Inserisci nell'ordine i seguenti parametri:");
		socketOut[i].flush();
		socketOut[i].println("Tipologia tessera terreno(MAIUSCOLO), Nome_Utente");
		socketOut[i].flush();
		
		EnumRegione tipologia = EnumRegione.valueOf(socketIn[i].nextLine());/*ERRORE A RUN TIME*/
		String nome_Utente = socketIn[i].nextLine();
		
		Iterator<Giocatore> iteratorGiocatori = strutturaDati.giocatori.iterator();
		while(iteratorGiocatori.hasNext()){
			giocatore = iteratorGiocatori.next();
			if(giocatore.getNome() == nome_Utente){
				break;
			}
		}
	
		return controlloAzioniPartita.controlloCompraTessereTerreno(tipologia, giocatore);
	}
	
	/**
	 * Funzione che richiede i paramtri per eseguire il metodo controllaMuoviPastore() che a sua volta richiamarà
	 * in caso di verifica corretta delle precondizioni il metodo muoviPastore() di AzioniPartita.
	 * 
	 * @param i: indice del vettore socketVettore contenente l'oggetto Socket per comunicare con il client corretto
	 * @return: true se controllaMuoviPastore() ritorna true (mossa eseguita), false altrimenti (la mossa non puo
	 * essere eseguita)
	 * 
	 *  @author Valerio Ferretti e Alberto Marini
	 */
	public boolean richiediParametriMuoviPastore(int i){
		
		Giocatore giocatore = null;
		Strada stradaPartenza = null;
		Strada stradaArrivo = null;
		
		socketOut[i].println("Inserisci nell'ordine i seguenti parametri:");
		socketOut[i].flush();
		socketOut[i].println("Id_Strada_Partenza, Id_Strada_Arrivo, Nome_Utente");
		socketOut[i].flush();
		
		int id_Strada_Partenza = socketIn[i].nextInt();
		int id_Strada_Arrivo = socketIn[i].nextInt();
		String nome_Utente = socketIn[i].nextLine();
		int posizione = socketIn[i].nextInt();
		
		Iterator<Giocatore> iteratorGiocatori = strutturaDati.giocatori.iterator();
		while(iteratorGiocatori.hasNext()){
			if(iteratorGiocatori.next().getNome() == nome_Utente){
				giocatore = iteratorGiocatori.next();
				break;
			}
		}
		
		Iterator<Strada> iteratorStrade1 = strutturaDati.strade.iterator();
		while(iteratorStrade1.hasNext()){
			if(iteratorStrade1.next().getId() == id_Strada_Partenza){
				stradaPartenza = iteratorStrade1.next();
				break;
			}
		}
		
		Iterator<Strada> iteratorStrade2 = strutturaDati.strade.iterator();
		while(iteratorStrade2.hasNext()){
			if(iteratorStrade2.next().getId() == id_Strada_Arrivo){
				stradaArrivo = iteratorStrade2.next();
				break;
			}
		}
		
		return controlloAzioniPartita.controllaMuoviPastore(stradaPartenza, stradaArrivo, giocatore, posizione);
	}
	
	public void calcolaVinciore(){
		
		int sommaVincente = 0;
		int vincitore = 0;
		
		azioniPartita.calcolaPunteggioFinale();
		
		Iterator<Giocatore> iteratorGiocatori = strutturaDati.giocatori.iterator();
		for(int i = 0; iteratorGiocatori.hasNext(); i++ ){
			Giocatore giocatore = iteratorGiocatori.next();
			if(giocatore.getSoldi() > sommaVincente){
				sommaVincente = giocatore.getSoldi();
				vincitore = i;
			}
		}
		
		socketOut[vincitore].println("Hai vinto la partita!");
		socketOut[vincitore].flush();
		for(int i = 0; i < strutturaDati.getNumeroGiocatori(); i ++){
			if(i != vincitore){
				socketOut[i].println("Hai perso!");
				socketOut[i].flush();
			}
		}
	}
	
}