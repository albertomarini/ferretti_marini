package it.polimi.ferretti_marini.Rete;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

/**
 * Contiene i metodi che dialogano tramite socket con i metodi di Server_Socket per fornire ad essi le informazioni
 * digitate dal client necessarie per il loro corretto funzionamento. Tali metodi stampano inoltre i messagi ricevuti
 * dal server tramite socket per i client
 * 
 * @author Valerio Ferretti e Alberto Marini
 *
 */
public class ClientSocket {
	
	private String ip;
	private int port;
	private Socket socket;
	private Scanner socketIn;
	private PrintWriter socketOut;
	private Scanner stdIn;
	private int numeroGiocatoriAdOraConnessi;
	private int numeroGiocatori;
	private boolean fineGioco = false;
	
	public ClientSocket(String ip, int port){
		this.ip = ip;
		this.port = port;
	}

	public int getNumeroGiocatori() {
		return numeroGiocatori;
	}

	public void setNumeroGiocatori(int numeroGiocatori) {
		this.numeroGiocatori = numeroGiocatori;
	}

	public int getNumeroGiocatoriAdOraConnessi() {
		return numeroGiocatoriAdOraConnessi;
	}

	public void setNumeroGiocatoriAdOraConnessi(int numeroGiocatoriAdOraConnessi) {
		this.numeroGiocatoriAdOraConnessi = numeroGiocatoriAdOraConnessi;
	}

	/*Prima del turno di ogni giocatore verra invocata questa funzione per determinare quele pastore dell'oggetto
	  giocatore deve essere utilizzato*/
	public int selezionaPastore(){
		
		Scanner input = new Scanner(System.in);
		
		if(getNumeroGiocatori() == 2){
			System.out.println("0 per il selezionare il primo pastore, 1 per selezionoare il secondo");
			int i = Integer.parseInt(input.nextLine());
			input.close();
			return i;
		} else{
			input.close();
			return 0;
		}
	}

	/*Inizializza la connessione, crea il socket con cui comunicare con il server, stampa su terminale 
	  del client il messaggio connessione riuscita inviatogli dal server e il numero di giocatori attualmente
	  connessi, in caso di giocatori >2 decide se precedere con il gioco o attenderne altri*/
	public void inizializzaConnessioneSocketClient() throws UnknownHostException, IOException{
		
		/*Inizializzazione connessione e creazione stream I/O*/
		socket = new Socket(ip, port);
		stdIn = new Scanner(System.in);
		socketIn = new Scanner(socket.getInputStream());
		socketOut = new PrintWriter(socket.getOutputStream());
		String string = socketIn.nextLine();
		System.out.printf("%s\n", string);

		/*Aggiornamento tramite notifica del server del numero di giocatori attualmente connessi*/
		setNumeroGiocatoriAdOraConnessi(Integer.parseInt(socketIn.nextLine()));
		System.out.printf("Il numero di giocatori finora connessi è pari a %d\n", numeroGiocatoriAdOraConnessi);
		
		/*In caso di un numero di giocatori >2 l'utente decide se iniziare la partita o aspettarne altri*/
		if(getNumeroGiocatoriAdOraConnessi() > 1 && getNumeroGiocatoriAdOraConnessi() != 4){
			
				System.out.println(socketIn.nextLine());
				socketOut.println(stdIn.nextLine());
				socketOut.flush();
			}
		setNumeroGiocatori(Integer.parseInt(socketIn.nextLine()));
	}
	
	public void inizzializzaGiocatore(){
		
		/*Invia al server informazioni riguardo il colore scelto e il nome del giocatore*/
		System.out.println(socketIn.nextLine());
		socketOut.println(stdIn.nextLine());
		socketOut.flush();
		socketOut.println(stdIn.nextLine());
		socketOut.flush();
		System.out.println(socketIn.nextLine());
		
		/*Ricezione dal server conferme corrette inizializzazione dei dati restanti*/
		System.out.println(socketIn.nextLine());
		System.out.println(socketIn.nextLine());
		System.out.println(socketIn.nextLine());
		System.out.println(socketIn.nextLine());
	}
	
	public void esecuzioneTurnoUtenteSocket(){
		
		boolean mossaEseguita = false;
		
		/*For per consentire di eseguire 3 mosse valide*/
		for(int j = 0; j < 3; j ++){
			
			mossaEseguita = false;
			/*While per richiedere la mossa finche questa non è valida*/
			while(!mossaEseguita){
			
				/*Messaggi dal server in cui si richiede di scegliere la propria mossa e invio mossa*/
				System.out.println(socketIn.nextLine());
				System.out.println(socketIn.nextLine());
				
				int mossa = Integer.parseInt(stdIn.nextLine());

				socketOut.println(mossa);
				socketOut.flush();
			
				System.out.println(socketIn.nextLine());
				
				/*A seconda della mossa prescelta passagio alle funzioni che richiedono i parametri dei parametri
				  necessari */
				
					System.out.println(socketIn.nextLine());
					System.out.println(socketIn.nextLine());

				if(mossa == 0){
					socketOut.println(stdIn.nextLine());
					socketOut.flush();
					socketOut.println(stdIn.nextLine());
					socketOut.flush();
					socketOut.println(stdIn.nextLine());
					socketOut.flush();
					socketOut.println(selezionaPastore());
					socketOut.flush();
				} else if(mossa == 1){
					socketOut.println(stdIn.nextLine());
					socketOut.flush();
					socketOut.println(stdIn.nextLine());
					socketOut.flush();
				} else if(mossa == 2){
					socketOut.print(stdIn.nextInt());
					socketOut.print(stdIn.nextInt());
					socketOut.println(stdIn.nextLine());
					socketOut.print(selezionaPastore());
				}
			
				/*Aggiornamento di mossaEseguita per sapere se è necessario iterare perche la mossa non è valida o
			  	si puo procedere con la mossa successiva*/
			
				mossaEseguita = Boolean.valueOf(socketIn.nextLine());
				System.out.printf("Il valore della variabile mossaEseguita è %b\n", mossaEseguita);
			
				/*Stampa esito mossa*/
			
				System.out.println(socketIn.nextLine());
			}
		
			fineGioco = Boolean.valueOf(socketIn.nextLine());
			System.out.printf("Il valore della variabile fineGioco è %b", fineGioco);
			if(fineGioco){
				System.out.println("I recinti finali sono terminati. Calcolo punnteggio ...");
				break;
			}
		}
	}
	
	public void stampaVincitore(){
		
		System.out.println("Partita terminata!");
		System.out.println(socketIn.nextLine());
	}
	
}